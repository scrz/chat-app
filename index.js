let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
app.get('/', (request, response) => {
    response.sendFile(__dirname + '/index.html')
})
http.listen(1000, () => {
    console.log('You have connected')
})
io.on('connection', (socket) => {
    console.log('Вы подключились к сокету!')
    socket.on('disconnect', () => {
        console.log('Вы отключились от сокета!')
    })
    socket.on('chat-message', (data) => {
        socket.broadcast.emit('chat-message', (data))
    })
    socket.on('chat-enter', (data) => {
        socket.broadcast.emit('chat-enter', (data))
    })
    socket.on('chat-exit', (data) => {
        socket.broadcast.emit('chat-exit', (data))
    })
    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', (data))
    })
    socket.on('stopTyping', (data) => {
        socket.broadcast.emit('stopTyping', (data))
    })

})